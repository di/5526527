  #!/usr/bin/python
  
  def z(a, c):
      r[a] = 0
      return c + 1
  
  def i(a, c):
      r[a] += 1
      return c + 1
  
  def j(a, c):
      if r[a[0]] != r[a[1]]:
          return a[2]
      return c + 1
  
  # Initialize an empty register
  r = [None for _ in range(10)]
  
  # Set the initial values
  r[0] = 3
  r[1] = 4
  
  # Instruction set
  b = [(z,2), (z,3), (z,4), (z,5), (z,6), (i,6),
       (j,(0,3,8)), (j,(5, 6, 11)),
       (i, 3), (i, 2), (j,(0,3,8)),
       (j,(1,4,13)), (j,(5, 6, 17)),
       (i, 4), (i, 2), (j,(1,4,13))]
  
  # Initialize the counter to the first instruction
  counter = 0
  
  # Start through the instruction set
  while counter < len(b):
      f, n = b[counter]
      counter = f(n, counter)
  
  # Print the register at the end of the simulation
  print(r)
  
  # Print the resulting computation
  print("%d + %d = %d" % (r[0], r[1], r[2]))